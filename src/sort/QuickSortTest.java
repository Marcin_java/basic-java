package sort;

import org.junit.Test;

import static org.junit.Assert.*;

public class QuickSortTest {

    @Test
    public void sort() {
        int[] numbers = {9, 5, 2, 3, 6, 1, 7};
        int[] sortedNumbers = {1, 2, 3, 5, 6, 7, 9};

        QuickSort quickSort = new QuickSort();
        quickSort.sort(numbers);
        assertArrayEquals(numbers, sortedNumbers);

    }
}