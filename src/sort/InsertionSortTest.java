package sort;

import org.junit.Test;

import static org.junit.Assert.*;

public class InsertionSortTest {

    @Test
    public void sort() {
        int[] numbers = {9, 5, 2, 3, 6, 1};
        int[] sortedNumbers = {1, 2, 3, 5, 6, 9};
        InsertionSort.sort(numbers);
        
        assertArrayEquals(sortedNumbers, numbers);
    }
}