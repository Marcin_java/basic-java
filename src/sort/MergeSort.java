package sort;

public class MergeSort {

    private int[] numbers;
    private int[] helper;

    public void sort(int[] values) {
        this.numbers = values;
        this.helper = new int[numbers.length];
        mergesort(0, numbers.length - 1);

    }

    private void mergesort(int low, int high) {
        // jezeli indeks startowy jest mniejszy od indeksu koncowego
        // to tablica ma wiecej niz 1 element
        // czyli mozna ja podzielic
        // jezeli tablica bedzie miala 1 element (nie da sie rozdzielac)
        // to low bedzie 0 i high tez bedzie 0
        if (low < high) {
            // oblicz srodkowy indeks
            int middle = low + (high - low) / 2;
            // posortuj lewa czesc tablicy
            mergesort(low, middle);
            // posortuj prawa czesc tablicy
            mergesort(middle + 1, high);
            // scalaj
            merge(low, middle, high);

        }
    }

    // Algorytm scalania dwoch tablic
    private void merge(int low, int middle, int high) {
        // przekopiowanie do pomocniczej
        for (int i = low; i <= high; i++) {
            helper[i] = numbers[i];
        }
        // poczatek lewej tablicy
        int i = low;
        int j = middle + 1; // poczatek prawej tablicy
        // do wpisywania do scalonej tablicy - zaczynamy od lewej
        int k = low;
        // tak dlugo az dojdziemy do konca jednej z tablic
        // middle wyznacza koniec lewej tablicy
        // jezeli i albo j dojdzie do konca
        // to jeden z warunkow zwroci falsz
        // wiec alternartywa bedzie falszywa
        while (i <= middle && j <= high) {
            // element z lewej strony jest mniejszy
            if (helper[i] <= helper[j]) {
                numbers[k] = helper[i];
                i++;
            } else { // element z prawej strony jest mniejszy
                numbers[k] = helper[j];
                j++;
            }
            k++;
        }
        // skopiuj reszte z lewej tablicy
        while (i <= middle) {
            numbers[k] = helper[i];
            i++;
            k++;
        }
    }
}
