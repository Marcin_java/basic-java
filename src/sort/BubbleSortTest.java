package sort;

import org.junit.Test;

import static org.junit.Assert.*;

public class BubbleSortTest {


    @Test
    public void shouldSortArray() {
        int[] array = {9, 5, 7, 12, 3};
        BubbleSort.sort(array);
        System.out.println();
    }

    @Test
    public void shouldSortStrings() {
        String[] texts = {"Piotr", "Jan", "Krzysztof", "Ania", "Marta", "Kasia", "Alina"};
        BubbleSort.sortString(texts);
        System.out.println();
    }
}