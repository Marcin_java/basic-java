package sort;

// zal. liczby z przedzialu 0 - max
public class BucketSort {

    public static void sort(int[] arr) {
        // znalezc max
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        // wywolac metode
        sort(arr, max);
    }

    private static void sort(int[] arr, int max) {
        // utworzyc tablice na kubelki
        int[] bucket = new int[max + 1];

        // przejsc for'em przez tablice arr
        // i zliczac ilosc wystapien elementow
        for (int i = 0; i < arr.length; i++) {
            // pobierz liczbe z tablicy
            int liczba = arr[i];
            // do koszyka o indeksie odpowiadajacym tej liczbie zwieksz
            // ich licznosc o 1
            bucket[liczba] += 1;
//            bucket[indeks]++; //to samo co wyzej
//            II opcja
//            bucket[arr[i]]++;
        }

        int k = 0;
        //zewnetrzna petla przechodzi przez tablice koszykow
        for (int i = 0; i < bucket.length; i++) {
            // wewnetrzna petla wykonuje sie tyle razy,
            // ile bylo elementow w koszyku
            for (int j = 0; j < bucket[i]; j++) {
                arr[k] = i;
                k++;
            }
        }
    }
}
