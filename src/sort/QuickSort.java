package sort;

public class QuickSort {
    private int[] numbers;

    public void sort(int[] values) {
        // sprawdzenie, czy tablica nie jest pusta
        if (values == null || values.length == 0) {
            return;
        }
        // jezeli doszlismy tutaj, to tablica nie jest nullem
        // i posiada elementy (nie jest pusta)
        this.numbers = values;
        quicksort(0, numbers.length - 1);
    }

    private void quicksort(int low, int high) {
        // i -> wskazuje na poczatek lewej listy
        int i = low;
        // j -> wskazuje na koniec prawej
        int j = high;
        //wybierz srodkowy element
        int midIndex = (low + high) / 2;
        int pivot = numbers[midIndex];
        // przejdz lewa czesc i prawda czesc listy
        // wykonuj tak dlugo az ideksy lewej i prawej polowy sie zejda
        while (i <= j) {
            // szukamy elementu wiekszego od piwotu z lewej czesci
            // i mniejszego z prawej
            while (numbers[i] < pivot) {
                i++;
            }
            while (numbers[j] > pivot) {
                j--;
            }
            if (i <= j) {
                // zamien miejscami i oraz j
                int helper = numbers[i];
                numbers[i] = numbers[j];
                numbers[j] = helper;
                i++;
                j--;
            }
        }
        if (low < j) {
            quicksort(low, j);
        }
        if (i < high) {
            quicksort(i, high);
        }


    }
}
