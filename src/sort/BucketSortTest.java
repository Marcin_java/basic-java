package sort;

import org.junit.Test;

import static org.junit.Assert.*;

public class BucketSortTest {

    @Test
    public void sort() {
        int[] numbers = {9, 9, 5, 5, 3, 2, 3, 6, 1};
        int[] sortedNumbers = {1, 2, 3, 3, 5, 5, 6, 9, 9};
        BucketSort.sort(numbers);
        assertArrayEquals(sortedNumbers, numbers);
    }
}