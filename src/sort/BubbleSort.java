package sort;

// Zlozonosc czasowa O(n^2)
// Zlozonosc pamieciowa O(1)
public class BubbleSort {
    /**
     * Sortuje tablice rosnaco
     *
     * @param array
     */
    public static void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length - i; j++) {
                // porownanie dwoch elementow
                // jezeli warunek jest prawdziwy,
                // to nalezy zamienic elementy
                if (array[j] < array[j - 1]) {
                    // zamien elementy w tablicy
                    int helper = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = helper;
                }
            }
        }
    }

    public static void sortString(String[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length - i; j++) {
                if (array[j - 1].compareTo(array[j]) > 0) {
                    String helper = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = helper;
                }
            }
        }
    }
}
