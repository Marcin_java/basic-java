package sort.measure;

import pl.sda.poznan.sort.*;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Program wypelniajacy losowymi wartosciami tablice
 * i mierzacy czasy sortowania
 * z wykorzystaniem zaimplementowanych algorytmow sortowania
 */
public class Program {
    public static int[] getRandomArray(int size) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100000);
        }
        return array;
    }

    public static void main(String[] args) {
        // sout -> podaj ile elementow wylosowac
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ile elementow wylosowac");
        int arraySize = scanner.nextInt();
        // utworzyc tablice o takim rozmiarze
        // wypelnic ja losowymi wartosciami
        int[] array = getRandomArray(arraySize);

        //insertion sort
        insertionSortExample(array);
        //bucket sort
        bucketSortExample(array);

        // posortowac mergeSortExample - wyswietlic czas
        mergeSortExample(array);

        // przyklad sortowania szybkiego
        quickSortExample(array);

        // posortowac babelkowo - wyswietlic czas
        bubbleSortExample(array);

    }

    private static void insertionSortExample(int[] array) {
        System.out.println("\nSortowanie przez wstawianie: ");
        int[] toSort = Arrays.copyOf(array, array.length);
        System.out.println("Rozpoczynam sortowanie.......");
        long startTime = System.currentTimeMillis();
        InsertionSort.sort(toSort);
        long endTime = System.currentTimeMillis();
        long bubbleSortTime = endTime - startTime;
        System.out.println("Zakonczono sortowanie przez wstawianie.... Czas to: " + bubbleSortTime);
        System.out.println();
    }

    private static void bubbleSortExample(int[] array) {
        System.out.println("\nSortowanie babelkowe: ");
        int[] toSort = Arrays.copyOf(array, array.length);
        System.out.println("Rozpoczynam sortowanie.......");
        long startTime = System.currentTimeMillis();
        BubbleSort.sort(toSort);
        long endTime = System.currentTimeMillis();
        long bubbleSortTime = endTime - startTime;
        System.out.println("Zakonczono sortowanie.... Czas to: " + bubbleSortTime);
        System.out.println();
    }

    private static void bucketSortExample(int[] array) {
        System.out.println();
        System.out.println("Sortowanie kubelkowe: ");
        int[] toSort = Arrays.copyOf(array, array.length);
        System.out.println("Rozpoczynam sortowanie.......");
        long startTime = System.currentTimeMillis();
        BucketSort.sort(toSort);
        long endTime = System.currentTimeMillis();
        long sortTime = endTime - startTime;
        System.out.println("Zakonczono sortowanie.... Czas to: " + sortTime);
        System.out.println();
    }


    private static void mergeSortExample(int[] array) {
        System.out.println();
        System.out.println("Sortowanie przez scalanie");
        int[] toSortByMerge = Arrays.copyOf(array, array.length);
        System.out.println("Rozpoczynam sortowanie.......");
        MergeSort mergeSort = new MergeSort();
        long startTimeInMillis = System.currentTimeMillis();
        mergeSort.sort(toSortByMerge);
        long endTimeInMillis = System.currentTimeMillis();
        long mergeSortResult = endTimeInMillis - startTimeInMillis;
        System.out.println("Zakonczono sortowanie.... Czas to: " + mergeSortResult);
        System.out.println();
    }

    private static void quickSortExample(int[] array) {
        System.out.println();
        System.out.println("Sortowanie szybkie");
        int[] toSort = Arrays.copyOf(array, array.length);
        System.out.println("Rozpoczynam sortowanie...");
        QuickSort quickSort = new QuickSort();
        long startTimeInMillis = System.currentTimeMillis();
        quickSort.sort(toSort);
        long endTimeInMillis = System.currentTimeMillis();
        long result = endTimeInMillis - startTimeInMillis;
        System.out.println("Zakonczono sortowanie.... Czas to: " + result);
        System.out.println();
    }
}
