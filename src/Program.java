

import pl.sda.poznan.collections.array.ArrayListCustom;

import java.util.LinkedList;
import java.util.List;

public class Program {
    public static void main(String[] args) {
        String s1 = "Ala";
        String s2 = "Krzysztof";

        int wynik = s1.compareTo(s2);
        if (wynik < 0) {
            System.out.println("Napis s1 jest mniejszy - powinien byc wczesniej w slowniku");
        } else if (wynik > 0) {
            System.out.println("Napis s2 jest wiekszy ");
        } else {
            System.out.println("napisy sa rowne");
        }

        Person student = new Person();
        student.setSurname("Kowalski");
        student.setAge(20);

        Person kierownik = new Person();
        kierownik.setSurname("Adamiak");
        kierownik.setAge(50);

        int result = student.compareTo(kierownik);
        System.out.println("Wynik porownania to: " + result);

    }
}
