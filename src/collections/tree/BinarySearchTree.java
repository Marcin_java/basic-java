package collections.tree;

import pl.sda.poznan.Person;

// Klasa BinarySearchTree jest typu generycznego,
// czyli moze pracowac z dowolnym typem
// typem, ktory implmentuje interfejs Comparable
// i klasa BinarySearchTree implementuje interfejs Tree
// <T extends Comparable<T>>  - ograniczenie przyjmowanego typu generycznego
// do takiego ktory implementuje interfejs Comparable
// dzieki temu zapisowowi na obiekcie root mozemy wywolac metode compareTo()
public class BinarySearchTree<T extends Comparable<T>> implements Tree<T> {

    private Node<T> root;

    @Override
    public void insert(T data) {
        // jesli drzewo jest puste to pierwszy dodawany element staje sie korzeniem
        if (root == null) {
            this.root = new Node<>(data);
        } else {
            insert(data, root);
        }
    }

    private void insert(T data, Node<T> node) {
        // porownujemy wstawiany element z danym wezlem
        // jezeli data.compareTo(node.getData) zwroci wartosc ujemna
        // to znaczy, ze musi trafic do lewego poddrzewa (jest mniejszy)
        // w przeciwnym przypadku do prawego
        if (data.compareTo(node.getData()) < 0) {
            // wstawiamy do lewego podrzewa
            if (node.getLeft() != null) {
                // jezeli dany wezel ma lewego syna,
                // to nie mozemy wstawic w to miejsce
                // musimy rekurencyjnie wywolac jeszcze raz ta funkcje (zejsc w dol drzewa)
                insert(data, node.getLeft());
            } else {
                // wezel nie ma lewego syna, wiec wstawiamy w to miejsce
                Node<T> newNode = new Node<>(data);
                node.setLeft(newNode);
            }
        } else {
            if (node.getRight() != null) {
                insert(data, node.getRight());
            } else {
                Node<T> newNode = new Node<>(data);
                node.setRight(newNode);
            }

        }
    }

    @Override
    public void delete(T data) {
        if (root != null) {
            root = delete(root, data);
        }
    }

    private Node<T> delete(Node<T> node, T data) {
        if (node == null) {
            return null;
        }
        // jezeli kasowany element jest mniejszy od wezla, to szukaj go w lewym poddrzewie
        if (data.compareTo(node.getData()) < 0) {
            node.setLeft(delete(node.getLeft(), data));
        } else if (data.compareTo(node.getData()) > 0) {
            //jezeli kasowany element jest wiekszy to szukaj go w prawym poddrzewie
            node.setRight(delete(node.getRight(), data));
        } else {
            // znalezlismy element do usuniecia
            // 3 przypadki
            // usuwanie liscia
            if (node.getLeft() == null && node.getRight() == null) {
                System.out.println("Removing leaf....");
                return null;
            }
            // usuwanie rodzica z 1 dzieckiem
            if (node.getLeft() == null) {
                System.out.println("Removing right.... ");
                Node<T> tempNode = node.getRight();
                node = null;
                return tempNode;
            } else if (node.getRight() == null) {
                System.out.println("Removing left ...");
                Node<T> tempNode = node.getLeft();
                node = null;
                return tempNode;
            }
            // usuwanie rodzica z 2 nastepnikami
            System.out.println("Remocing with 2 items");
            Node<T> tempNode = getPredecessor(node.getLeft());
            node.setData(tempNode.getData());
            node.setLeft(delete(node.getLeft(), tempNode.getData()));
        }

        return node;
    }

    private Node<T> getPredecessor(Node<T> node) {
        if (node.getRight() != null) {
            return getPredecessor(node.getRight());
        }
        return node;

        // alternatywny zapis:
//        if (node.getRight() == null) {
//            return node;
//        } else {
//            return getPredecessor(node.getRight());
//        }
    }

    @Override
    public T getMax() {
        // sprawdzenie czy mamy elementy w drzewie
        if (root == null) {
            return null;
        }
        // sprawdzamy, czy mamy prawy element
        // jezeli korzen nie ma prawego elemetu
        // to korzen jest najwiekszym elementem
        if (root.getRight() == null) {
            return root.getData();
        } else {
            // w przeciwnym wypadku
            // musimy zejsc w dol drzewa
            // wiec wywolujemy funkcje getMax z prawym synem
            return getMax(root.getRight());
        }
    }

    // pomocnicza metoda, ktora otrzymuje element drzewa jako argument
    // i sprawdza, czy ma prawego syna -> wtedy kontynuuje rekrencje,
    // az do znalezienia elementu bez prawego syna
    // element bez prawego syna jest najwiekszym elementem
    private T getMax(Node<T> node) {
        if (node.getRight() != null) {
            return getMax(node.getRight());
        }
        return node.getData();
    }

    @Override
    public T getMin() {
        if (root == null) {
            return null;
        }
        if (root.getLeft() == null) {
            return root.getData();
        } else {
            return getMin(root.getLeft());
        }
    }

    private T getMin(Node<T> node) {
        if (node.getLeft() == null) {
            return node.getData();
        } else {
            return getMin(node.getLeft());
        }
    }

    @Override
    public void traversal() {
        if (root != null) {
            System.out.println("INORDER");
            inorderTraversal(root);
//            System.out.println("PREORDER");
//            preorderTraversal(root);
//            System.out.println("POSTORDER");
//            postorderTraversal(root);
        }
    }

    @Override
    public boolean contains(T data) {
        // jezeli drzewo jest puste -> to na pewno nie ma szukanego elementu
        return contains(root, data);
    }

    private boolean contains(Node<T> node, T data) {
        if (node == null) {
            return false;
        }
        // znalezlismy szukany element - zwracamy true
        if (data.compareTo(node.getData()) == 0) {
            return true;
        } else if (data.compareTo(node.getData()) < 0) {
            // w przeciwnym przypadku, jezeli szukany element jest mniejszy od pola danych aktualnego wezla
            // to szukaj w jego lewym poddrzewie
            return contains(node.getLeft(), data);
        } else {
            // w przeciwnym przyopadku element jest wiekszy -> szukaj w prawym poddrzewie
            return contains(node.getRight(), data);
        }
    }

    /**
     * Przejscie in order przez drzewo - najpierw lewy syn, potem odwiedz korzen, potem odwiedz prawego syna
     *
     * @param node - tree node
     */
    private void inorderTraversal(Node<T> node) {
        // odwiedz lewego syna
        if (node.getLeft() != null) {
            inorderTraversal(node.getLeft());
        }
        // odwiedz rodzica
        System.out.println(node.getData().toString());
        // odwiedz prawego syna
        if (node.getRight() != null) {
            inorderTraversal(node.getRight());
        }

    }

    private void preorderTraversal(Node<T> node) {
        System.out.println(node.getData().toString());
        // odwiedz lewego
        if (node.getLeft() != null) {
            preorderTraversal(node.getLeft());
        }
        // odwiedz prawego
        if (node.getRight() != null) {
            preorderTraversal(node.getRight());
        }
    }

    private void postorderTraversal(Node<T> node) {
        if (node.getLeft() != null) {
            postorderTraversal(node.getLeft());
        }
        if (node.getRight() != null) {
            postorderTraversal(node.getRight());
        }
        System.out.println(node.getData().toString());

    }

}
