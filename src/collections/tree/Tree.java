package collections.tree;

public interface Tree<T> {
    /**
     * Metoda do dodawania elementu do drzewa
     *
     * @param data
     */
    void insert(T data);

    /**
     * Metoda do usuwania elementu z drzewa
     *
     * @param data
     */
    void delete(T data);

    /**
     * Metoda znajdujaca najwiekszy element
     *
     * @return
     */
    T getMax();

    /**
     * Metoda znajdujaca najmniejszy element w drzewie
     *
     * @return
     */
    T getMin();

    /**
     * Przejscie przez drzewo
     */
    void traversal();

    /**
     * Odpowiedz na pytanie czy element znajduje sie w drzewie
     */
    boolean contains(T data);
}
