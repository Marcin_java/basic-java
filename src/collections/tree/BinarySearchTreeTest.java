package collections.tree;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class BinarySearchTreeTest {
    Tree<Integer> tree = new BinarySearchTree<>();

    @Test
    public void insert() {
        tree.insert(15);
        tree.insert(10);
        tree.insert(20);
        tree.insert(5);
        tree.insert(13);
        tree.insert(18);
        tree.insert(23);
        tree.traversal();
    }

    @Test
    public void insertRandomValues() {
        Random random = new Random();
        for (int i = 0; i < 50; i++) {
            tree.insert(random.nextInt(100));
        }
        tree.traversal();
    }

    @Test
    public void shouldFindMaxElement() {

        tree.insert(15);
        tree.insert(10);
        tree.insert(20);
        tree.insert(5);
        tree.insert(13);
        tree.insert(18);
        tree.insert(23);
        int max = tree.getMax();
        assertEquals(23, max);
    }

    @Test
    public void shouldFindMinElement() {
        tree.insert(15);
        tree.insert(10);
        tree.insert(20);
        tree.insert(5);
        tree.insert(13);
        tree.insert(18);
        tree.insert(23);
        int min = tree.getMin();
        assertEquals(5, min);
    }

    @Test
    public void shouldDeleteLeaf() {
        tree.insert(15);
        tree.insert(10);
        tree.insert(20);
        tree.insert(5);
        tree.insert(13);
        tree.insert(18);
        tree.insert(23);
        tree.traversal();
        tree.delete(5);
        tree.traversal();
    }

    @Test
    public void shouldDeleteElementWithOneChild() {
        tree.insert(15);
        tree.insert(10);
        tree.insert(20);
        tree.insert(13);
        tree.insert(18);
        tree.insert(23);
//        tree.insert(1);
        tree.traversal();
        tree.delete(10);
        tree.traversal();
    }

    @Test
    public void shoudRemoveRoot() {
        tree.insert(15);
        tree.insert(10);
        tree.insert(20);
        tree.insert(5);
        tree.insert(13);
        tree.insert(18);
        tree.insert(23);
        tree.traversal();
        tree.delete(15);
        tree.traversal();

    }

    @Test
    public void shoudContainElement() {
        tree.insert(15);
        tree.insert(10);
        tree.insert(20);
        tree.insert(5);
        tree.insert(13);
        tree.insert(18);
        tree.insert(23);
        boolean contains = tree.contains(23);
        boolean notContains = tree.contains(50);
        assertEquals(true, contains);
        assertEquals(false, notContains);

    }
}