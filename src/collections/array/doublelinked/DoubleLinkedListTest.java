package collections.array.doublelinked;

import org.junit.Test;
import pl.sda.poznan.collections.array.generic.GenericList;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class DoubleLinkedListTest {
    private GenericList<String> list = new DoubleLinkedList<>();

    @Test
    public void shouldAddFirstElement() {
        list.add("Pierwszy");
        assertEquals(1, list.size());


    }

    @Test
    public void shouldAddManyElements() {
        list.add("Pierwszy");
        list.add("Drugi");
        list.add("Trzeci");
        assertEquals(3, list.size());
    }

    @Test
    public void shouldGetIndexOfElement() {
        list.add("Pierwszy");
        list.add("Drugi");
        list.add("Trzeci");
        list.add("Czwarty");
        list.add("Piaty");

        assertEquals(0, list.indexOf("Pierwszy"));
        assertEquals(1, list.indexOf("Drugi"));
        assertEquals(2, list.indexOf("Trzeci"));
        assertEquals(3, list.indexOf("Czwarty"));
        assertEquals(4, list.indexOf("Piaty"));
    }

    @Test
    public void shouldRemoveFromEnd() {
        list.add("Pierwszy");
        list.add("Drugi");
        list.add("Trzeci");
        String s = list.remove();
        assertEquals(2, list.size());
        assertEquals(s, "Trzeci");
    }

    @Test
    public void shouldRemoveFromBeginning() {
        // Poslugujemy sie typem klasu (a nie interfejsu),
        // poniewaz metoda zostala zdefiniowana w klasie
        // a nie w interfejsie
        DoubleLinkedList<String> linkedList = new DoubleLinkedList<>();
        linkedList.add("Pierwszy");
        linkedList.add("Drugi");
        linkedList.add("Trzeci");
        String deleted = linkedList.removeFromBeginning();
        assertEquals(2, linkedList.size());
        assertEquals(deleted, "Pierwszy");

    }

    @Test
    public void shouldReturnFalseWhenElementIsNotPresent() {
        list.add("First");
        list.add("Second");

        boolean result = list.remove("Zero");
        assertEquals(false, result);
    }

    @Test
    public void shouldRemoveElement() {
        list.add("First");
        list.add("Second");
        list.add("Third");
        list.add("Fourth");
        list.add("Fifth");
        boolean result = list.remove("Second");
        assertEquals(result, true);
        assertEquals(4, list.size());
        assertEquals(false, list.contains("Second"));


        LinkedList<Integer> integers = new LinkedList<>();
        integers.remove(1);

    }
}