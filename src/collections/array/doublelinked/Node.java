package collections.array.doublelinked;

/**
 * Klasa Node do reprezentacji listy podwojnie powiazanej
 * Ten element ma wskazania do poprzedniego i nastepnego elementu
 * (nie mylic z klasa Node z pakietu  pl.sda.poznan.collections.array.linkedlist
 * - to jest wezel do listy jednokierunkowej)
 */
public class Node<E> {
    private E data;
    private Node<E> prev;
    private Node<E> next;

    public Node(E data) {
        this.data = data;
    }

    public Node<E> getPrev() {
        return this.prev;
    }

    public void setPrev(Node<E> prev) {
        this.prev = prev;
    }

    public E getData() {
        return this.data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public Node<E> getNext() {
        return next;
    }

    public void setNext(Node<E> next) {
        this.next = next;
    }

}
