package collections.array.doublelinked;

import pl.sda.poznan.collections.array.generic.GenericList;

public class DoubleLinkedList<E> implements GenericList<E> {
    private Node<E> first;
    private Node<E> last;
    private int size;

    // zaimplementuj size, isEmpty, indexOf
    // potem dodawanie, usuwanie itd.


    public DoubleLinkedList() {
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    @Override
    public boolean add(E element) {
        // rozpatrzec przypadek dodawania pierwszego elementu
        // kolejnych elementow - dodawanie na koniec listy
        if (first == null) {
            // Tworzymy nowy wezel
            Node<E> newNode = new Node<>(element);
            first = newNode;
            last = newNode;
        } else {
            // Utworz nowy wezel
            Node<E> newNode = new Node<>(element);
            // do aktulnie ostatniego elementu do jego nastpenika wpisz nowyElement
            last.setNext(newNode);
            // w nowym elemencie poprzednik ustaw na stary koniec listy
            newNode.setPrev(last);
            // zaktualizuj wskaznik last na nowy element,
            // poniewaz to nowy element staje sie koncem listy
            last = newNode;
        }
        size++;
        return true;
    }

    // Napisac metode zwracajaca wezel po zawartosci elementu
    private Node<E> getNodeByElement(E element) {
        Node<E> helper = first;
        while (helper != null) {
            // wezel w tej iteracji to szukany wezel,
            // bo pole danych zgadza sie z wartoscia szukanego elementu
            if (helper.getData().equals(element)) {
                return helper;
            } else {
                // w przeciwnym przypadku -> iterujemy dalej
                helper = helper.getNext();
            }
        }
        return null;
    }

    @Override
    public void add(int index, E element) {
        // TODO : Implement this

    }

    @Override
    public boolean remove(E element) {
        // Sprawdzamy, czy element wystepuje, jezeli nie wystepuje zwracamy false
        if (!contains(element)) {
            return false;
        }
        Node<E> nodeToRemove = getNodeByElement(element);
        if (nodeToRemove == first) {
            removeFromBeginning();
        } else if (nodeToRemove == last) {
            remove();
        } else {
            // Przypadek usuwania elementu ktory jest w srodku
            // pobierz poprzednik usuwanego elementu
            Node<E> prevNode = nodeToRemove.getPrev();
            //Pobierz nastepnik usuwanego elementu
            Node<E> nextNode = nodeToRemove.getNext();

            // popraw wiazania
            prevNode.setNext(nextNode);
            nextNode.setPrev(prevNode);
            // lub -> krotsza forma
//        nodeToRemove.getPrev().setNext(nodeToRemove.getNext());
//            nodeToRemove.getNext().setPrev(nodeToRemove.getPrev());
            size--;
        }
        return true;
    }

    @Override
    public E remove(int index) {
        E elementToDelete = get(index);
        remove(elementToDelete);
        return elementToDelete;
    }

    @Override
    public void clear() {
        //TODO : Implement this
    }

    @Override
    public E get(int index) {
        //TODO : Implement this
        return null;
    }

    @Override
    public E remove() {
        //Pobierz wartosc ostatniego elementu
        E value = last.getData();
        //Przestaw referencje last na przedostatni element
        last = last.getPrev();
        // nastepnik ostatniego elementu musi wskazywac na null
        last.setNext(null);
        //zmniejsz rozmiar listy
        size--;
        //zwroc wartosc usuwanego elementu
        return value;
    }

    public E removeFromBeginning() {
        size--;
        E data = first.getData();
        first = first.getNext();
        first.setPrev(null);
        return data;
    }

    @Override
    public int indexOf(E element) {
        int index = 0;
        Node<E> helper = first;
        while (helper != null) {
            if (helper.getData().equals(element)) {
                return index;
            }
            helper = helper.getNext();
            index++;
        }
        return -1;
    }
}
