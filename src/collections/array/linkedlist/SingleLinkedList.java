package collections.array.linkedlist;

import pl.sda.poznan.collections.array.generic.GenericList;

/**
 * Implmenetacja listy jednokierunkowej z dowiazaniami
 * W tej implementacji mamy wskazanie na glowe listy
 * Ostatni element listy ma wskazanie next ustawione na null - po tym poznajemy koniec listy
 *
 * @param <E>
 */
public class SingleLinkedList<E> implements GenericList<E> {

    private Node<E> head;
    private int size;

    public SingleLinkedList() {
        this.size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }


    @Override
    public boolean add(E element) {
        size++;
        // 1. Lista jest pusta (if)
        // 2. Lista juz ma elementy (else)
        if (head == null) {
            //obslugujemy przypadek pustej listy - wstawiamy pierwszy element
            // jezeli lista jest pusta, to stworz nowy wezel i ustaw referencje HEAD na nowy wezel - koniec
            head = new Node<E>(element);
        } else {
            // wywolaj metode dodawania na poczatek lub na koniec - wybierz jedna wersje - dodawanie na poczatek lub na koniec
            insertDataAtBeggining(element); // wstawianie na poczatek listy
//            insertDataAtEnd(element); // wstawianie na koniec listy
        }
        return false;
    }

    // O(1) - stala zlozonosc czasowa - dodawanie na poczatek listy zajmuje zawsze ten sam czas


    // mamy jeden element: struktura wskaznikow:     HEAD -> NULL
    // O(1)
    private void insertDataAtBeggining(E element) {
        // Utworz nowy wezel
        Node<E> newNode = new Node<>(element);
        // Ustaw nastepnik nowego wezla na stara wskazanie glowy
        newNode.setNext(head);
        // zaktualizuj/przestaw stara wartosc glowy na nowy element
        head = newNode;

        // zalozenie: dodajemy drugi element
        // newNode(HEAD) -> OLD HEAD -> NULL
    }

    // O(n), gdzie n to rozmiar listy
    // poniewaz musimy za kazdym razem przejsc przez cala liste
    private void insertDataAtEnd(E element) {
        // zmienna pomocnicza do iterowania po liscie - na poczatku ustawiamy ja na glowe listy
        // helper i head wskazuja na poczatek listy
        // zmienna helper jest potrzebna, aby nie stracic referencji do HEAD
        //teraz helper i head pokazuja na pierwszy element listy
        Node<E> helper = head;

        // sprawdz czy element ma nastepnik -> jezeli tak to przestaw helper na nastepnik
        // w przeciwnym przypadku dotarlismy do konca listy -> mozemy wstawiac element
        while (helper.getNext() != null) {
            helper = helper.getNext();
        }

        // wstawianie elementu
        // utworz nowy wezel
        // teraz helper pokazuje na aktualnie ostatni element, wiec ustaw wskaznik next ostatniego elementu na newNode
        Node<E> newNode = new Node<>(element);
        helper.setNext(newNode);

        // newNode staje sie ostatnim elementem listy -> newNode (ma wskaznik next ustawiony na NULL)
    }

    @Override
    public void add(int index, E element) {
        Node<E> current = getNodeByIndex(index);
        if (current == head) {
            size++;
            insertDataAtBeggining(element);
            return;
        }
        Node<E> prev = getNodeByIndex(index - 1);
        Node<E> newNode = new Node<>(element);
        prev.setNext(newNode);
        newNode.setNext(current);
        size++;


    }

    /**
     * Prywatna metoda do zwracania wezla po indeksie
     *
     * @param index
     * @return
     */
    private Node<E> getNodeByIndex(int index) {
        Node<E> helper = head;
        int i = 0;
        while (i != index) {
            helper = helper.getNext();
            i++;
        }
        return helper;
    }

    /**
     * W tej implementacji kazdy element mam wskazanie tylko na nastpeny element.
     * Więc potrzebujemy dwoch referencji - jednej na znalezienie elementu do usuniecia
     * Drugiej referencji na element poprzedzajacy
     * Wiec helper ustawiamy na glowe, a prev na null (poniewaz poprzednik glowy ma wartosc null)
     *
     * @param element
     * @return
     */
    @Override
    public boolean remove(E element) {
        // jezeli nie mamy takiego elemetu w kolekcji to zwroc falsz
        if (!contains(element)) {
            return false;
        }
        Node<E> helper = head;
        Node<E> prev = null;
        while (helper != null) {
            if (helper.getValue().equals(element)) {
                break;
            }
            prev = helper;
            helper = helper.getNext();
        }
        //Jezeli poprzednik wynosi null -> oznacza to, ze mamy skasowac glowe
        // Wiec referencje head przestawiamy o jeden element dalej
        // w przeciwnym przypadku w poprzeniku usuwanego elementu, ustawiamy jego nastepnik
        // na wartosc nastepnika aktualnie usuwanego elementu
        if (prev == null) {
            head = helper.getNext();
        } else {
            prev.setNext(helper.getNext());
        }
        // wstaw null do referencji helper -> GC usunie ten obiekt z pamieci
        helper = null;
        size--;
        return true;
    }

    @Override
    public E remove(int index) {
        E element = this.get(index);
        this.remove(element);
        return element;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;
    }

    @Override
    public E get(int index) {
        // sprawdzenie czy operujemy na dobrym zakresie
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Wrong index");
        }
        // jezeli mamy jakis element to szukamy go na liscie
        // przechodzac przez jego wezly
        int i = 0;
        Node<E> helper = head;
        //wykonuj tak dlugo az znajdziesz indeks szukanego elementu
        while (i != index) {
            helper = helper.getNext();
            i++;
        }
        // zwroc wartosc z listy
        return helper.getValue();
    }

    @Override
    public E remove() {
//        E elemToDelete = get(size - 1);
//        remove(elemToDelete);
//        return elemToDelete;

        return remove(size - 1);
    }

    @Override
    public int indexOf(E element) {
        Node<E> helper = head;
        int index = 0;
        // dopoki nie dojdziesz do konca listy, wykonuj:
        while (helper != null) {
            if (helper.getValue().equals(element)) {
                return index;
            }
            helper = helper.getNext();
            index++;
        }
        return -1;

//      Analogiczny zapis z uzyciem petli for
//        for (int i = 0; i < size; i++) {
//            if (helper.getValue().equals(element)) {
//                return i;
//            }
//            helper = helper.getNext();
//        }
//        //// ... logika wyszukiwania
//
//
//        return -1;
    }

    public void print() {
        Node<E> helper = head;

        while (helper.getNext() != null) {
            System.out.println(helper.getValue().toString());
            helper = helper.getNext();
        }
        System.out.println(helper.getValue().toString());
    }
}
