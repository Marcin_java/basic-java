package collections.array.generic;

import org.junit.Test;
import pl.sda.poznan.Person;

import java.util.Scanner;

public class GenericArrayListTest {

    @Test
    public void size() {
        GenericList<Person> anotherPersonList = new GenericArrayList<>();
        Person piotr = new Person("");
        anotherPersonList.add(piotr);
        Person person = anotherPersonList.get(0);
        System.out.println(person);
        GenericList<Integer> genericList = new GenericArrayList<Integer>();

        int liczba = 5;
        Integer wiek = 5;

        double wynik;
        Double aDouble;

        boolean prawda;
        Boolean falsz;


        int mojWiek = 99;
        GenericList<Integer> lista = new GenericArrayList<>();
        lista.add(mojWiek);


        Integer cena = 699;
        float v = cena.floatValue();

        Integer.compare(5,6);

        Integer i = Integer.parseInt("5");

        System.out.println(i);

        Integer wynikObliczen = 5; // boxing
        int wartoscPrymitywna = wynikObliczen;  // unboxing

    }

    @Test
    public void boxingTest(){

    }
}