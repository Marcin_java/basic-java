package collections.array.generic;

import pl.sda.poznan.collections.Collection;

public interface GenericList<E> extends Collection<E> {

    void add(int index, E element);

    boolean remove(E element);

    E remove(int index);
    
    E get(int index);

    int indexOf(E element);
}
