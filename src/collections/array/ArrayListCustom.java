package collections.array;

import java.util.Arrays;

/**
 * Lista wykorzystujaca tablice (implementacja podobna do klasy java.util.ArrayList
 * Lista przechowuje wartosci typu String
 * Lista powinna miec nastepujace funkcje:
 * - zwracanie rozmiaru listy
 * - odpowiedz na pytanie czy lista pusta
 * - Dodawanie elementu na koniec listy
 * - Dodawanie w okreslonej pozycji
 * - Usuwanie po okreslonej wartosci
 * - Wyczyszczenie wszystkich wartosci
 * - Pobieranie po indeksie
 * - Usuwanie po indeksie
 */
public class ArrayListCustom implements MyList {
    //w tej tablicy bedziemy przechowywac elementy listy
    private String[] values;
    //zmienna okreslajaca ile mamy rzeczywiscie elementow na liscie
    private int size;
    // poczatkowa wartosc - jezeli bedzie przekroczona to nalezy skopiowac tablice do innej (np. dwa razy wiekszej)
    private static final int INITIAL_SIZE = 20;

    // konstruktor bezparametrowy - wykorzystamy go, gdy z gory nie bedziemy znali liczby elementow
    public ArrayListCustom() {
        this.size = 0;
        this.values = new String[INITIAL_SIZE];
    }

    // konstruktor przyjmujacy rozmiar - wydajniejsza opcja, jezeli z gory wiemy, ile elementow potrzebujemy
    public ArrayListCustom(int size) {
        this.size = 0;
        this.values = new String[size];
    }


    @Override
    public int size() {
        return this.size;           // 1
//        return values.length;        // 2 ?
        //Wybieramy opcje 1 - ona zwroci faktyczna ilosc.
        // Opcja 2 po utworzeniu nowej Listy zwroci dlugosc 20, mimo, ze jeszcze tam nie bedzie zadnych elementow
//
    }

    @Override
    public boolean isEmpty() {
        return size == 0;

// Analogiczny zapis do powyzszego
//        if(size == 0){
//            return true;
//        }else{
//            return false;
//        }
    }

    @Override
    public boolean contains(String o) {
        // Krotszy zapis (niz w pkt 3 nizej) -> zwroc czy indeks szukanego elementu jest wiekszy badz rowny 0
        // Jezeli to jest prawda to zwrocona bedzie wartosc true
        return indexOf(o) >= 0;

        // 1. Napisanie petli for - ktora sprawdza wszystkie elementy i zwraca true, jesli szukany element wystepuje
        // 2. Zauwazenie, ze to samo robi metoda indexOf(String) - mozemy z niej skorzystac

        // Analogiczny (dluzszy zapis do powyzszego)
        // Jezeli szukany element znajduje sie na pozycji 0 lub wiekszej od zero - to na pewno znajduje sie na liscie
        // Wiec zwracamy true
        // w przeciwnym przypadku zwracamy false
        //3.
//        if(indexOf(o) >= 0){
//            return true;
//        }else {
//            return false;
//        }
    }

    // Metoda do dodawania elementow do listy
    @Override
    public boolean add(String e) {
        //jezeli mamy pelna tablice
        //przekopiuj stara tablice do nowej dwa razy wiekszej
        if (size == values.length) {
            values = Arrays.copyOf(values, size * 2);
        }
        values[size++] = e;
        return true;
    }

    @Override
    public void add(int index, String element) {
        // sprawdzic indeks nie wykracza poza granice tablicy
        checkRange(index);
        //jezeli mamy pelna tablice
        //przekopiuj stara tablice do nowej dwa razy wiekszej
        if (size == values.length) {
            values = Arrays.copyOf(values, size * 2);
        }
        // przekopiuj reszte elementow do pomocniczej tablicy
        String[] restOfArray = Arrays.copyOfRange(values, index, size);
        // wstaw nowy element w miejscu index
        values[index] = element;
        size++;
        // przekopiuj stare wartosci na nowe pozycje
        int j = 0;
        for (int i = index + 1; i < size; i++) {
            values[i] = restOfArray[j];
        }
        // nie zapominaj o zmiennej size !
    }

    @Override
    public boolean remove(String o) {
        // mamy usunac element podany jako argument
        // wiec funkcja indexOf sprawdzamy jaki ma index
        // mamy funkcje ktora usuwa element po indeksie
        // wiec majac indeks wywolujemy funkje do kasowania po indeksie
        // funkcja ta zwraca usuniety element
        // wiec jezeli ten element jest rozny od null
        // to zakladamy ze kasowanie sie udalo i wynikiem tej funkcji bedzie true
        int i = indexOf(o);
        String deleted = remove(i);
        return deleted != null;
    }

    @Override
    public void clear() {
        // 1. Ustawienie rozmiaru na 0
        // i utworzenie nowej tablicy
        // stara bedzie usunieta w procesie odsmiecania przez mechanizm Garbage Collectora (GC)
        // ustawiamy rozmiar na 0
        size = 0;
        // zmienna sluzaca do przechwycenia starej wielkosci tablicy
        int newSize = values.length;
        // przypisanie null spowoduje, ze tracimy referencje do oryginalnej tablicy
        values = null; // (opcjonalne) - to samo zrobi instrukcja ponizej
        //do referecnji values przypisujemy nowa tablice, ktora bedzie utworzona w nowym obszarze pamieci
        // stara tablica zostanie automatycznie usunieta przez GC
        values = new String[newSize];

        // 2. sposob
        // przejscie z uzyciem petli for
        // i wpisanie wartosci null do kazdego z elementow
//        for (int i = 0; i < size; i++) {
//            values[i] = null;
//        }
//        size = 0;

    }

    @Override
    public String get(int index) {
        checkRange(index);
        return this.values[index];
    }


    @Override
    public String remove(int index) {
        //pomocnicza metoda prywantna do sprawdzania prawidłowego zakresu tablicy
        checkRange(index);
        String toDelete = values[index];
        values[index] = null;

        // obsluga przerwy (rozdzielenia) tablicy
        String[] restOfArray = Arrays.copyOfRange(values, index + 1, size);
        size--;
        // kopiowanie reszty tablicy do oryginalnej tablicy
        int j = 0;
        for (int i = index; i < size; i++) {
            values[i] = restOfArray[j++];
        }
        values[size] = null;
        return toDelete;
    }

    // Pobierz ostatni element do zmiennej pomocniczej toDelete
    // do tablicy o ostatnim indeksie wpisz null
    // zmniejsz rozmiar o 1
    // zwroc usuniety element
    @Override
    public String removeFromEnd() {
        int indexOfLastElemet = size - 1;
        String toDelete = values[indexOfLastElemet];
        values[indexOfLastElemet] = null;
        size--;
        return toDelete;

//        String toDelete = values[size];
//        values[size--] = null;
//        return toDelete;
    }

    /**
     * Metoda zwracajaca pozycje napisu s w liscie
     * Jezeli element wystepuje to zwracamy jego index
     * Jezeli element nie wystepuje to zwracamy -1
     * Na pewno indeks w tablicy nie bedzie ujemny, wiec mozemy zwrocic -1
     *
     * @param s - szukany element
     * @return
     */
    @Override
    public int indexOf(String s) {
        for (int i = 0; i < size; i++) {
            if (values[i].equals(s)) {
                return i;
            }
        }
        return -1;
    }

    private void checkRange(int index) {
        if (index < 0 || index >= size) {
//            return null;
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }
}
