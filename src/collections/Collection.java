package collections;

public interface Collection<E> {
    int size();

    boolean isEmpty();

    boolean contains(E element);

    boolean add(E element);

    E remove();

    void clear();
}
