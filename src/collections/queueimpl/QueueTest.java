package collections.queueimpl;

import org.junit.Test;
import pl.sda.poznan.collections.Collection;

import static org.junit.Assert.*;

public class QueueTest {

    private Collection<String> stringQueue = new Queue<>();

    @Test
    public void shouldAddElements() {
        stringQueue.add("First");
        stringQueue.add("Second");
        stringQueue.add("Third");
        stringQueue.add("Fourth");

        assertEquals(4, stringQueue.size());
    }

    @Test
    public void shouldDeleteElement() {
        stringQueue.add("First");
        stringQueue.add("Second");
        stringQueue.add("Third");
        stringQueue.add("Fourth");

        String deleted = stringQueue.remove();
        assertEquals("First", deleted);
        assertEquals(3, stringQueue.size());
    }
}