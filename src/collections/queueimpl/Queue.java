package collections.queueimpl;

import pl.sda.poznan.collections.Collection;

/**
 * Kolejka - struktura danych, gdzie dodajemy elementy na koncu,
 * a usuwamy na poczatku
 * Kolejka to lista FIFO - First In First Out
 * Element ktory trafi do kolejki jako pierwszy,
 * jako pierwszy bedzie obslzony
 * Ostatni element, bedzie obsluzony na koncu
 *
 * @param <E>
 */
public class Queue<E> implements Collection<E> {
    private int size;
    private Node<E> head;
    private Node<E> tail;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(E element) {
        return false;
    }

    @Override
    public boolean add(E element) {
        Node<E> newNode = new Node<>(element);
        if (head == null) {
            head = newNode;
            tail = newNode;
        } else {
            tail.setNext(newNode);
            newNode.setPrev(tail);
            tail = newNode;
        }
        size++;
        return true;
    }

    @Override
    public E remove() {
        Node<E> toDelete = head;
        head = head.getNext();
        head.setPrev(null);
        size--;
        return toDelete.getData();
    }

    @Override
    public void clear() {

    }
}
