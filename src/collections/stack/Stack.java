package collections.stack;

/**
 * Stos - struktura danych, gdzie dodajemy na wierzcholek stosu
 * i usuwamy (zdejmujemy) elementy z wierzcholka
 * Przyklad listy LIFO - Last In First Out
 * Element dodany na koncu, bedzie obsluzony jako pierwszy
 *
 * @param <E>
 */
public class Stack<E> {
    private Node<E> top;
    private int size = 0;


    // Odkladanie elementu na stos
    // Zawsze odkladamy na szczyt stosu
    public void push(E element) {
        size++;
        Node<E> newNode = new Node<>(element);
        newNode.setPrev(top);
        top = newNode;

        // Wersja alternatywna
//        if (top == null) {
//            top = new Node<>(element);
//        } else {
//            Node<E> newNode = new Node<>(element);
//            newNode.setPrev(top);
//            top = newNode;
//        }
    }

    // Pobieranie elementu ze stosu
    // Zawsze pobieramy szczytowy element
    // wiec metoda nie przyjmuje argumentu
    public E pop() {
        if (top == null) {
            throw new ArrayIndexOutOfBoundsException("Stack is empty");
        }
        --size;
        Node<E> toDelete = top;
        top = top.getPrev();
        return toDelete.getData();
    }

    public boolean isEmpty() {
        return size == 0;
    }

    // Usuwa wszystkie elementy ze stosu
    public void clear() {
        top = null;
        size = 0;
    }

    // Zwraca element top, bez usuwania
    public E peek() {
        return isEmpty() ? null : top.getData();

        // Alternatywny zapis
//        if (top == null) {
//            return null;
//        }
//        return top.getData();
//

    }

    // metoda ma zwracac czy element wystepuje na stosie
    // Jezeli tak to ile elementow od elementu top
    // Jezeli szukany element jest na szycie to zwrocic 1
    // Jezeli drugi (zaraz po szczycie to zwrocic 2 itd).
    // Jezeli nie to zwracamy -1
    public int search(E element) {
        return -1;
    }

    public int size() {
        return this.size;
    }
}
