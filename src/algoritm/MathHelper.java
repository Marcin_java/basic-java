package algoritm;

public class MathHelper {
    /**
     * Obliczanie silni rekurencyjnie
     *
     * @param n
     * @return
     */
    public static int factorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("n must be greater than 0");
        }
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    /**
     * Rekurencyjne obliczanie n-tego wyrazu ciagu Fibonacciego
     *
     * @param n
     * @return
     */
    public static int fibonacci(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }
}
