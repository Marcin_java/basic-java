package jvm;

import pl.sda.poznan.Person;

public class PassArgumentToMethodExample {

    // przekazywany przez wartosc

    // pass by value
    public static void multiply(int value) {
        System.out.println("2. " + value); // 5
        value = value * 10;
        System.out.println("3. " + value); // 50
    }

    public static void changeAge(Person person) {
        System.out.println(person.getAge()); // 20
        person.setAge(100);
        System.out.println(person.getAge()); // 100
    }

    public static void changeStudent(Person person) {
        System.out.println(person.getAge()); // 20
        person = new Person();
        person.setAge(100);
        System.out.println(person.getAge()); // 100
    }

    public static void main(String[] args) {
        // Primitive types
        /*int a = 5;
        System.out.println("1. " + a); // ? 5
        multiply(a);
        System.out.println("4. " + a); // 50?    5*/

        /*Person student = new Person();
        student.setAge(20);
        System.out.println(student.getAge()); // 20

        changeAge(student);

        System.out.println(student.getAge()); // 20 vs 100 ?*/

        Person student = new Person();
        student.setAge(20);
        System.out.println(student.getAge()); // 20

        changeStudent(student);

        System.out.println(student.getAge()); // 20 vs 100 ?

    }
}
