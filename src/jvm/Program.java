package jvm;

import java.util.List;

public class Program {

    public static final String PATH_TO_FILE = "C:\\Users\\admin\\IdeaProjects\\sda-basic-programming\\src\\plik.txt";

    // polaczyc wszystkie napisy z listy
    // w 1 napis
    private static String concatanateStringsBadExample(List<String> list) throws InterruptedException {
        String result = null;
        // logic here ....
        for (String line : list) {
            result += line;
//            Thread.sleep(10);
        }

        return result;
    }

    private static String concatanateStringsWithBuilder(List<String> list) throws InterruptedException {
        StringBuilder builder = new StringBuilder();
        for (String line : list) {
            builder.append(line).append("\n");
            Thread.sleep(10);
        }
        return builder.toString();
    }


    public static void main(String[] args) throws InterruptedException {
        List<String> lines = FileOperation.readLinesFromFile(PATH_TO_FILE);
        System.out.println("Wczytano: " + lines.size() + " lini...");
        String wszystkieLinie = concatanateStringsWithBuilder(lines);
        System.out.println();
    }
}
